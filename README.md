## FSCI Gemdeps Generator

This repo uses GitLab's CI/CD pipelines to generate dependency information of Ruby applications whose packaging is done by FSCI members (under Debian Ruby team). All the information necessary is stored in `softwares.yml` file. Modify that file, add and commit it, and push it. The push will trigger a pipeline that will generate the necessary json files that the service at https://debian-ruby.gitlab.io/gemdeps-generator use.

Also, the pipelines are auto-run every Sunday at 12:00 AM IST.

Note 1: This used to be served from http://debian.fosscommunity.in earlier
Note 2: It is also available at https://new-gemdeps.balasankarc.in 

